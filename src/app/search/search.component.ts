import { Component, OnInit } from '@angular/core';
import { Course } from '../models/course';
import { CoursesService } from '../services/courses.service';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  courses: Course[];
  searchCourses: Course[];
  test: any[] = [];

  constructor(private courseService: CoursesService) { }

  ngOnInit() {
    this.courseService.getCourses().subscribe(res => this.courses = res );
  }

  onInscriptionClick(idCourse: number, sessionNum: number) {
    if (!this.sessionChecked(idCourse, sessionNum)) {
      this.test.push({id: idCourse, session: sessionNum});
    }
    console.log('test', this.test);
  }

  sessionChecked(idCourse: number, sessionNum: number) {
    return this.test.some(course => course.id === idCourse && course.session === sessionNum);
  }


  onSearchChange(word: string) {
    this.searchCourses = [];
    this.courses.forEach(course => {
      if (course.titre.toUpperCase().includes(word.toUpperCase())) {
        this.searchCourses.push(course);
      }
    });
  }


}
