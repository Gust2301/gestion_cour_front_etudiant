export interface Session {
    id: number;
    date_debut: Date;
    duree_semaines: number;
}
