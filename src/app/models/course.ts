import { Session } from './session';

export interface Course {
    id: number;
    titre: string;
    author: string;
    resume: string;
    connaissance: string;
    numSession: number;
    sessions: Session[];
}
